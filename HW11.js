let form = document.querySelector('.password-form');
form.setAttribute('id', 'formElements');
let elements = formElements.elements;
let value1 = '';
let value2 = '';
form.addEventListener('click', (event) => {
    if (event.target.classList.contains('fa-eye')) {
        event.target.classList.remove('fa-eye');
        event.target.classList.add('fa-eye-slash');
        event.target.previousElementSibling.setAttribute('type', 'text');
    } else if (event.target.classList.contains('fa-eye-slash')) {
        event.target.classList.remove('fa-eye-slash');
        event.target.classList.add('fa-eye');
        event.target.previousElementSibling.setAttribute('type', 'password');
    }
    if (event.target.dataset.name == 'password1') {
        event.target.addEventListener('change', (event) => {
            value1 = event.target.value;
        })
    } else if (event.target.dataset.name == 'password2') {
        event.target.addEventListener('change', (event) => {
            value2 = event.target.value;
        }) 
    }
   /* якщо закоментувати івент з кнопкою і вставити той що прямо звертається - то видалення параграфа буде видалятись, а в самому івенті ні і не розумію причини */ 
let p;
if (event.target.classList.contains('btn')) {
    if (value1 !== value2 || value1 == "" || value2 == "") {
        let error = document.createElement('p');
        error.innerHTML =  'Потрібно ввести однакові значення';
        error.style.color = 'red';
        error.classList.add('active');
        event.target.before(error);
        p = document.querySelectorAll('.active');
        console.log(p.length, p)
        if (p.length >= 2) {
            p[1].remove();
        };
    } else if (value1 === value2) {
        alert('You are welcome');
        if (p[0]) {
         p[0].remove();
        };
        // хоч true, p.length == 1 вставити в if  - не працює
    }
    event.preventDefault();
}
})

// let p;
// elem[2].addEventListener('click', (event) => {
//     if (value1 !== value2 || value1 == "" || value2 == "") {
//         let error = document.createElement('p');
//         error.innerHTML =  'Потрібно ввести однакові значення';
//         error.style.color = 'red';
//         error.classList.add('active');
//         elem[2].before(error);
//         p = document.querySelectorAll('.active');
//         console.log(p.length)
//         if (p.length >= 2) {
//             p[1].remove();
//         };
//     } else if (value1 === value2) {
//         alert('You are welcome');
//         if (p[0]) {
//             p[0].remove();
//         };
//     }
//     event.preventDefault();
// })